<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <title>Elmstone Hotel & Spa</title>
    <meta name="description" content="Lorem ipsum dolor sit amet, consectetur adipiscing elit."/>
    <link rel="stylesheet" href="css/style.css?v=<?php echo hash_file('md5', 'css/style.css'); ?>">
    <link href="https://fonts.googleapis.com/css?family=Lato|PT+Serif&display=swap" rel="stylesheet">
</head>
<body>
    <header class="header">
        <nav>
            <ul>
                <li><a href="tel:01234567890"><img src="/assets/phone.svg" alt="Phone"/><span>01234 567 890</span></a></li>
                <li><a href="/"><img src="/assets/elmstone.svg" alt="Elmstone Hotel & Spa"/></a></li>
                <li><a href="mailto:enquiries@example.com"><img src="/assets/email.svg" alt="Email"/><span>enquiries@example.com</span></a></li>
            </ul>
        </nav>
    </header>
   <main class="main">
        <section class="intro">
            <div>
                <h1><small>Lorem Ipsum</small>Lorem ipsum dolor sit</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut luctus at sem ac sagittis. Pellentesque vel facilisis risus. Integer vitae sodales ex, quis ullamcorper dolor.</p>
                <a href="#" class="button primary">Book Now</a>
            </div>
        </section>

        <section class="articles">
            <div class="items">
                <div class="item">
                    <div class="item-image">
                        <img src="/assets/image1.jpg" alt="image" />
                    </div>
                    <div class="item-copy">
                        <h2><small>Lorem Ipsum</small>Lorem ipsum dolor sit amet</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut luctus at sem ac sagittis. Pellentesque vel facilisis risus. Integer vitae sodales ex, quis ullamcorper dolor. Pellentesque et eros nulla. Morbi in ligula eleifend, elementum mi sit amet, molestie lorem. Nunc aliquam ornare sodales.</p>
                        <a href="#" class="button primary">Book Now</a>
                        <a href="#" class="button secondary">More Info</a>
                    </div>
                </div>
                <div class="item">
                    <div class="item-image">
                        <img src="/assets/image2.jpg" alt="image" />
                    </div>
                    <div class="item-copy">
                        <h2><small>Lorem Ipsum</small>Lorem ipsum dolor sit amet</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut luctus at sem ac sagittis. Pellentesque vel facilisis risus. Integer vitae sodales ex, quis ullamcorper dolor. Pellentesque et eros nulla. Morbi in ligula eleifend, elementum mi sit amet, molestie lorem. Nunc aliquam ornare sodales.</p>
                        <a href="#" class="button primary">Book Now</a>
                        <a href="#" class="button secondary">More Info</a>
                    </div>
                </div>
            </div>
            
            
        </section>

        <section class="featured">
            <div class="items">
                <div class="item">
                    <div class="item-image">
                        <img src="/assets/image3.jpg" alt="image" />
                    </div>
                    <div class="item-copy">
                        <h3>Lorem ipsum dolor</h3>
                    </div>
                </div>
                <div class="item">
                    <div class="item-image">
                        <img src="/assets/image4.jpg" alt="image" />
                    </div>
                    <div class="item-copy">
                        <h3>Lorem ipsum dolor</h3>
                    </div>
                </div>
                <div class="item">
                    <div class="item-image">
                        <img src="/assets/image5.jpg" alt="image" />
                    </div>
                    <div class="item-copy">
                        <h3>Lorem ipsum dolor</h3>
                    </div>
                </div>
            </div>
        </section>
   </main>
</body>
</html>
