## How to get started:
1. Download and install Docker
    * Linux: "sudo apt install docker.io"
    * Windows: https://download.docker.com/win/stable/Docker%20for%20Windows%20Installer.exe
    * Mac: https://download.docker.com/mac/stable/Docker.dmg
2. Git clone this repository
3. Navigate to "elmstone-project"
4. Open terminal and execute this command "docker-compose up"
5. Navigate to "http://localhost:8080/"

## Services:
1. NGINX
2. PHP7+
3. SASS

## Useful commands:
View docker containers:
docker container ls

Web server bash command:
docker exec -it elmstone-project_web_1 bash

Watch logs:
docker logs -f elmstone-project_web_1
docker logs -f elmstone-project_php_1
